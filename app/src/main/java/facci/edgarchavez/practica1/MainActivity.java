package facci.edgarchavez.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonLogin, buttonBuscar, buttonGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = (Button) findViewById( R.id.buttonLogin );
        buttonBuscar = (Button) findViewById( R.id.buttonBuscar );
        buttonGuardar = (Button) findViewById( R.id.buttonGuardar );

        buttonGuardar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this, guardar.class );
                startActivity( intent );
            }
        } );

        buttonBuscar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this, buscar.class );
                startActivity( intent );
            }
        } );
        buttonLogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( MainActivity.this, login.class );
                startActivity( intent );

            }
        } );


    }

}
